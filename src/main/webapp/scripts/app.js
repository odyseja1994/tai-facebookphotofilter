'use strict';

/**
 * @ngdoc overview
 * @name facebookPhotoFilterApp
 * @description
 * # facebookPhotoFilterApp
 *
 * Main module of the application.
 */
angular.module('facebookPhotoFilterApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ui.router',
    'angularShiro',
    'ngTouch',
    'fbModule'
  ])
  .config(['$urlRouterProvider', '$stateProvider', 'angularShiroConfigProvider', function($urlRouterProvider, $stateProvider, config) {
    config.setLoginPath('/index');
    config.setLogoutPath('/index');
    config.setAuthenticateUrl('/rest/authenticate');
    console.log=console.error=console.debug=console.warn=console.info=function(){

    };

    $stateProvider
      .state('main', {
        url: '/',
        onEnter: ['$state', '$timeout', function ($state, $timeout) {
          $timeout(function () {
            $state.go('home');
          });
        }]
      }).state('home', {
        url: '/index',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        reload: true
      }).state('sendImage', {
        url: '/sendImage',
        templateUrl: 'views/sendimage.html',
        controller: 'SendimageCtrl',
        controllerAs: 'sendImage'
      }).state('login', {
        url: '/login',
        templateUrl: "views/login.html",
        controller: 'AuthCtrl'
      }).state('register', {
        url: '/register',
        templateUrl: "views/register.html",
        controller: "AuthCtrl"
      }).state('imageList', {
        url: '/imageList',
        templateUrl: "views/imagelist.html",
        controller: "ImageListCtrl"
      }).state('image', {
        url: '/image/:id',
        templateUrl: "views/image.html",
        controller: "ImageCtrl"
      });

    $urlRouterProvider.otherwise('/');
  }]);
angular.module('fbModule', ['facebook'])
  .config(function(FacebookProvider) {
     // Set your appId through the setAppId method or
     // use the shortcut in the initialize method directly.
     FacebookProvider.init('1105489226177442');
  });
