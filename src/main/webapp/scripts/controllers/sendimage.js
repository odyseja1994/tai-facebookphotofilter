'use strict';

/**
 * @ngdoc function
 * @name facebookPhotoFilterApp.controller:SendimageCtrl
 * @description
 * # SendimageCtrl
 * Controller of the facebookPhotoFilterApp
 */
angular.module('facebookPhotoFilterApp')
  .controller('SendimageCtrl', [
    '$scope',
    '$http',
    'subject',
    'usernamePasswordToken',
    '$location',
    '$filter',
    function ($scope, $http, subject, UsernamePasswordToken, $location, $filter ) {
    if(!subject.isAuthenticated()){
      $location.path('/');
    }
    $scope.newImage = null;

    $scope.onFileSelect = function ($file) {
      var reader = new FileReader();
      reader.onLoad = function (e) {
        console.log("about to encode");
        $scope.encoded_file = btoa(e.target.result.toString());
      };
      reader.readAsBinaryString($file);
    };

    $scope.byteSize = function (base64String) {
      if (!angular.isString(base64String)) {
        return '';
      }
      function endsWith(suffix, str) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
      }

      function paddingSize(base64String) {
        if (endsWith('==', base64String)) {
          return 2;
        }
        if (endsWith('=', base64String)) {
          return 1;
        }
        return 0;
      }

      function size(base64String) {
        return base64String.length / 4 * 3 - paddingSize(base64String);
      }

      function formatAsBytes(size) {
        return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
      }

      return formatAsBytes(size(base64String));
    };
    $scope.setPicture = function (file) {
      var fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = function (e) {

        var data = e.target.result;
        var base64Data = data.substr(data.indexOf('base64,') + 'base64,'.length);
        $scope.$apply(function () {
          $scope.newImage = base64Data;
          $scope.filename = file.name;
        });
      };
    };

    $scope.upload = function(){
      var imageToSend={'nickname': subject.getPrincipal().login, 'authToken': subject.getPrincipal().token, 'image': $scope.newImage};
      $http.post('/rest/images/create', imageToSend).then(function(){
        $location.path('/imageList');
      }, function(){
        $scope.error=true;
      });
    };
  }]);
