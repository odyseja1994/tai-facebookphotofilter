"use strict"

angular.module('facebookPhotoFilterApp')
  .controller('ImageCtrl', ['$scope',
    '$http',
    'Facebook',
    'subject',
    'usernamePasswordToken',
    '$location',
    '$stateParams',
    function ($scope, $http, Facebook, subject, usernamePasswordToken, $location, $stateParams ) {
      if(!subject.isAuthenticated()){
        $location.path('/');
      }
      $scope.originalId = $stateParams.id;
      $scope.result=null;

      // $scope.done = "/rest/" + $scope.selectedFilter + "/";

      var init = function () {
        $http.get('/rest/images/'+$scope.originalId+'/obj')
          .success(
            function (data) {
              $scope.image = data.image;
            });

        $http.get('/rest/filters')
          .success(
            function (data) {
              $scope.filters = data;
              //$scope.selectedFilter = null;
            });
      };

      $scope.makeFilter = function (sel) {
        if(sel=="null"){
          $scope.cancel();
        } else {
          $http.get("/rest/filters/" + sel + "/" + $scope.originalId)
            .success(
              function (data) {
                $scope.result = data;
              });
        }

      };

      $scope.update = function(sel) {
        $scope.selectedFilter = sel;
        if(sel=="null"){
          $scope.cancel();
        }
      };

      $scope.saveImage = function(){
        var imageToSend={'nickname': subject.getPrincipal().login, 'authToken': subject.getPrincipal().token, 'image': $scope.result.image};
        $http.post('/rest/images/create', imageToSend)
          .success(
            function (data) {
              $scope.imgId = data;
            }).then(function(){
                $location.path('/image/'+$scope.imgId)
            });
      };

      $scope.cancel = function(){
        $scope.result=null;
      };

      $scope.shareFb = function() {
        Facebook.login(function(response) {
          Facebook.ui({
              method: "feed",
              link: location.host + "/rest/images/" + $scope.originalId,
              caption: "TAI picture: " + $scope.originalId
            },
            function(response) {

            });

        });

      };

      init();

    }]);
