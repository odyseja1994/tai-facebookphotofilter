'use strict';

/**
 * @ngdoc function
 * @name facebookPhotoFilterApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the facebookPhotoFilterApp
 */
angular.module('facebookPhotoFilterApp')
  .controller('MainCtrl', ['$scope',
    '$rootScope',
    '$timeout',
    'subject',
    'usernamePasswordToken',
    '$location',
    '$filter',
    '$http',
    function ($scope, $rootScope, $timeout, subject, usernamePasswordToken, $location, $filter, $http) {

    $scope.register=true;
    $scope.changeView = function(){
      $scope.register = !$scope.register;
    };
  }]);
