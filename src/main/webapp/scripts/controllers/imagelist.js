"use strict"

angular.module('facebookPhotoFilterApp')
  .controller('ImageListCtrl', ['$scope',
    '$http',
    'Facebook',
    'subject',
    'usernamePasswordToken',
    '$location',
    function ($scope, $http, Facebook, subject, usernamePasswordToken, $location ) {

      if(!subject.isAuthenticated()){
        $location.path('/');
      }

      var currentUser = {"nickname": subject.getPrincipal().login, "authToken": subject.getPrincipal().token };

      var init = function () {
        $http.post('/rest/images', currentUser)
              .success(
                  function (data) {
                    $scope.images = data;
                  });

      };

      $scope.openImage = function(id){
        $location.path('/image/'+id)
      };


      init();

  }]);
