'use strict';

/**
 * @ngdoc function
 * @name facebookPhotoFilterApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the facebookPhotoFilterApp
 */
angular.module('facebookPhotoFilterApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
