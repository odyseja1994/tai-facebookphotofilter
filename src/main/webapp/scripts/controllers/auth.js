'use strict';

/**
 * @ngdoc function
 * @name facebookPhotoFilterApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the facebookPhotoFilterApp
 */
angular.module('facebookPhotoFilterApp')
  .controller('AuthCtrl', ['$scope',
    '$rootScope',
    '$timeout',
    'subject',
    'usernamePasswordToken',
    '$location',
    '$filter',
    '$http',
    function ($scope, $rootScope, $timeout, subject, usernamePasswordToken, $location, $filter, $http) {
      $scope.errauthc = false;
      $scope.token = usernamePasswordToken;
      $scope.confirmation = null;

      $scope.login = function() {
        subject.login($scope.token).then(function(result) {
          $scope.errauthc = false;
          $scope.token=usernamePasswordToken;
          $location.path('/imageList');
        }, function(data) {
          $scope.errauthc = true;
        });
      };

      $scope.register = function(confirmation){
        if($scope.token.password==confirmation){
          $http.post('/rest/users', {principal: $scope.token.username, credentials: $scope.token.password}).then(
            function(){
              $scope.token=usernamePasswordToken;
              $location.path("/login");
            },
            function(){
              $scope.errauthc=true;
            });
        }
      };

    }]);
