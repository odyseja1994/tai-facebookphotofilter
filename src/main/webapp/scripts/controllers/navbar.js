'use strict';

/**
 * @ngdoc function
 * @name facebookPhotoFilterApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the facebookPhotoFilterApp
 */
angular.module('facebookPhotoFilterApp')
  .controller('NavCtrl', ['$scope',
    '$rootScope',
    '$timeout',
    'subject',
    'usernamePasswordToken',
    '$location',
    '$filter',
    '$http',
    function ($scope, $rootScope, $timeout, subject, usernamePasswordToken, $location, $filter, $http) {

      $scope.errauthc = false;
      $scope.token = usernamePasswordToken;
      $scope.confirmation = null;

      $scope.$watch(function(){return subject.isAuthenticated()}, function(newVal, oldVal){
        $scope.authed = newVal;
      });

      $scope.logout = function(){
        console.log("logging out");
        $http.put('/rest/authenticate', {nickname: subject.getPrincipal().login, authToken: subject.getPrincipal().token}).then(function(){
          subject.logout();
          $location.path("/index");
        });
      };

}]);
