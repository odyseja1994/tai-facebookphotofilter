'use strict';

describe('Controller: SendimageCtrl', function () {

  // load the controller's module
  beforeEach(module('facebookPhotoFilterApp'));

  var SendimageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SendimageCtrl = $controller('SendimageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SendimageCtrl.awesomeThings.length).toBe(3);
  });
});
