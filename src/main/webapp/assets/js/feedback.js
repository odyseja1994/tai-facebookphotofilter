$(document).ready(function () {
  $('#send').click(function () {
    $('#send').hide();
    $('#success').fadeIn('slow');
    $('#message').val('');
    setTimeout(function(){
      $('#feedback').modal('hide');
    }, 2000);
  })
});
