$(function () {
  $('.nav.nav-pills li').click(function () {
    if (!$(this).hasClass('active')) {
      $('.nav.nav-pills li.active').removeClass('active');
      $(this).addClass('active');
    }
  });

  $(".panel1").hover(function () {
    $(this).css("overflow", "scroll");
  }, function () {
    $(this).css("overflow", "hidden");
  });

  $('#help-text-btn').click(function () {
    if ($('#text-editor').attr('aria-describedby'))
      return;
    $('#text-editor').focus()
      .popover('show')
      .blur(function () {
        $(this).popover('hide');
      });
  });

  $('a[data-link*="test-creator-"]').click(function () {
    var link = $(this).attr('data-link');
    var $activeTab = $('.active-tab');
    var $chosenTab = $(link);

    if (!$chosenTab.hasClass('active-tab')) {
      $activeTab.removeClass('active-tab').toggle();
      $chosenTab.addClass('active-tab').toggle();
    }
  });
});
