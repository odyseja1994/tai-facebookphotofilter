package pl.agh.ki.tai.photofilter.utils;

import lombok.Setter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

/**
 * Utility to save images to repository.
 */
public final class RepositoryUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryUtil.class);
    @Setter
    private static String repositoryPath = "repo";

    /**
     * private default constructor for utility class.
     */
    private RepositoryUtil() {
    }

    /**
     * Saves image to repository.
     * @param image image to save
     * @return path to imae if saved, null if exception occured
     */
    public static String saveImageToRepository(final byte[] image) {
        File repo = new File(repositoryPath);
        if (!repo.exists()) {
            repo.mkdirs();
        }
        int files = FileUtils.listFiles(repo, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE).size();
        String filename = files + ".png";
        File outputFile = new File(repo, filename);
        try {
            FileUtils.writeByteArrayToFile(outputFile, convertImageToPng(image));
            return outputFile.getPath();
        } catch (IOException e) {
            LOGGER.error("Cannot save image: {}", e);
            return null;
        }
    }

    /**
     * Converts image to png.
     * @param image image to convert
     * @return image in png
     */
    private static byte[] convertImageToPng(final byte[] image) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(image);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            ImageIO.write(ImageIO.read(inputStream), "png", output);
        } catch (IOException e) {
            LOGGER.error("Cannot convert image: {}", e);
        }
        return output.toByteArray();
    }
}
