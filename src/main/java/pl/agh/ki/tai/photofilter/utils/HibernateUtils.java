package pl.agh.ki.tai.photofilter.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import pl.agh.ki.tai.photofilter.model.Image;
import pl.agh.ki.tai.photofilter.model.User;

/**
 * Created by Ola on 2016-05-18.
 */
public final class HibernateUtils {
    private static SessionFactory sessionFactory;

    /**
     * private default constructor for utility class.
     */
    private HibernateUtils() {
    }

    /**
     * Prepares Session Factory for Hibernate if it doesn't exist.
     * @return sessionFactory
     */
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            String hibernateFile = "/hibernate.cfg.xml";
            Configuration configuration = new Configuration();
            configuration.addResource(hibernateFile);

            configuration.addAnnotatedClass(User.class)
                    .addAnnotatedClass(Image.class).configure();
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                    configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }
        return sessionFactory;
    }

    /**
     * Sets the static session factory.
     * @param sessionFactory session factory to set
     */
    public static void setSessionFactory(final SessionFactory sessionFactory) {
        HibernateUtils.sessionFactory = sessionFactory;
    }

    /**
     * Returns the session from Session Factory.
     * @return hibernate session
     */
    public static Session getSession() {
        return getSessionFactory().openSession();
    }

    /**
     * Saves object to database.
     * @param object to save
     * @return id of added object in database.
     */
    public static Integer saveObject(final Object object) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        Integer id = (Integer) session.save(object);
        transaction.commit();
        session.close();
        return id;
    }

    /**
     * Updates object in database.
     * @param object to update
     */
    public static void updateObject(final Object object) {
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(object);
        transaction.commit();
        session.close();
    }

    /**
     * Closes the session factory.
     */
    public static void shutdown() {
        getSessionFactory().close();
    }
}
