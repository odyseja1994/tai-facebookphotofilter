package pl.agh.ki.tai.photofilter.webapp.rest.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by aleksandra on 27.05.16.
 */
@Data
@XmlRootElement(name = "user")
public class UserDTO {
    @XmlElement
    private int id;
    @XmlElement
    private String nickname;
    @XmlElement
    private byte[] authToken;
    @XmlElement
    private String hashedPassword;
    @XmlElement
    private String email;
    @XmlElement
    private String fbToken;
}
