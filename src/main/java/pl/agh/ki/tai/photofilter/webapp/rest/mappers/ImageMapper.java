package pl.agh.ki.tai.photofilter.webapp.rest.mappers;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.agh.ki.tai.photofilter.model.Image;
import pl.agh.ki.tai.photofilter.webapp.rest.dto.ImageDTO;

import java.io.File;
import java.io.IOException;

/**
 * Converts Image and ImageDTO.
 */
public final class ImageMapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ImageMapper.class);

    /**
     * private default constructor for utility class.
     */
    private ImageMapper() {
    }


    /**
     * Converts Image to ImageDTO.
     * @param image Image
     * @return ImageDTO
     */
    public static ImageDTO mapImageToImageDto(final Image image) {
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setFilename(FilenameUtils.getBaseName(image.getPath()));
        imageDTO.setId(image.getId());
        try {
            imageDTO.setImage(FileUtils.readFileToByteArray(new File(image.getPath())));
        } catch (IOException e) {
            LOGGER.warn("Cannot read image: {}", e);
        }
        return imageDTO;
    }


}
