package pl.agh.ki.tai.photofilter.utils;

import org.apache.shiro.authc.SaltedAuthenticationInfo;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.apache.shiro.util.SimpleByteSource;

/**
 * Needed by Apache Shiro.
 */
public class MySaltedAuthenticationInfo implements SaltedAuthenticationInfo {
    private static final long serialVersionUID = -5467967895187234984L;

    private final String username;
    private final String password;
    private final String salt;

    /**
     * Constructor for MySaltedAuthenticationInfo.
     * @param username String
     * @param password String
     * @param salt String
     */
    public MySaltedAuthenticationInfo(final String username,
                                      final String password,
                                      final String salt) {
        this.username = username;
        this.password = password;
        this.salt = salt;
    }

    @Override
    public final PrincipalCollection getPrincipals() {
        return new SimplePrincipalCollection(username, username);
    }

    @Override
    public final Object getCredentials() {
        return password;
    }

    @Override
    public final ByteSource getCredentialsSalt() {
        return new SimpleByteSource(Base64.decode(salt));
    }

}
