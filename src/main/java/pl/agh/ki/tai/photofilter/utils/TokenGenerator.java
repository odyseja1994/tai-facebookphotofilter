package pl.agh.ki.tai.photofilter.utils;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;

/**
 * Random authentication token generator.
 */
public final class TokenGenerator {
    /**
     * private default constructor for utility class.
     */
    private TokenGenerator() {
    }

    /**
     * Generates random token.
     * @return random token
     */
    public static byte[] getToken() {
        return new SecureRandomNumberGenerator().nextBytes().getBytes();
    }
}
