package pl.agh.ki.tai.photofilter.webapp.rest;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.agh.ki.tai.photofilter.model.Token;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.model.dao.UserDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.HttpURLConnection;

/**
 * Created by aleksandra on 27.05.16.
 */
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserResource.class);
    private static final Integer HASH_ITERATIONS = 1024;


    /**
     * POST /rest/users.
     * @param token User to add
     * @return status 200 if added, 422 otherwise
     */
    @POST
    public final Response addUser(final Token token) {
        try {
            LOGGER.info("Got post with user: {}", token);
            User user = new User();
            user.setNickname(token.getPrincipal());
            generatePassword(user, token.getCredentials());
            UserDAO.saveUser(user);
            LOGGER.info("User: {}", user);

            return Response.status(HttpURLConnection.HTTP_CREATED).build();
        } catch (Exception e) {
            LOGGER.error("Exception occured: {}", e);
            return Response.status(HttpURLConnection.HTTP_BAD_REQUEST).build();
        }
    }

    /**
     * Generates hashed password.
     * @param user user to generate hashed password
     * @param plainTextPassword plain text password
     */
    private void generatePassword(final User user, final String plainTextPassword) {
        RandomNumberGenerator rng = new SecureRandomNumberGenerator();
        Object salt = rng.nextBytes();

        // Now hash the plain-text password with the random salt and multiple
        // iterations and then Base64-encode the value (requires less space than Hex):
        String hashedPasswordBase64
                = new Sha256Hash(plainTextPassword, salt, HASH_ITERATIONS).toBase64();
        user.setHashedPassword(hashedPasswordBase64);
        user.setSalt(salt.toString());
    }

}
