package pl.agh.ki.tai.photofilter.webapp.common;

import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ext.Provider;

/** Listener, for debugging purpose. **/
@Provider
public class ExceptionMapper implements ApplicationEventListener {

    @Override
    public void onEvent(final ApplicationEvent event) {

    }

    @Override
    public final RequestEventListener onRequest(final RequestEvent requestEvent) {
        return new ExceptionRequestEventListener();
    }

    /**
     * Exception listener, for debugging purpose.
     */
    public static class ExceptionRequestEventListener implements RequestEventListener {
        private final Logger logger = LoggerFactory.getLogger(ExceptionRequestEventListener.class);;

        @Override
        public final void onEvent(final RequestEvent event) {
            switch (event.getType()) {
                case ON_EXCEPTION:
                    Throwable t = event.getException();
                    logger.error("Found exception for requestType: {} {}", event.getType(), t);
                    break;
                default:
                    break;
            }
        }
    }
}
