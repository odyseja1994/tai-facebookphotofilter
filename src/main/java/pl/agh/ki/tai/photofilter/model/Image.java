package pl.agh.ki.tai.photofilter.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import java.util.List;

/**
 * Created by Ola on 2016-05-17.
 */
@Data
@Entity
@Table(name = "image")
public class Image {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "path")
    private String path;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "image")
    private List<Image> imagesWithFilters;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Enumerated(EnumType.ORDINAL)
    private Filter filter;

    @ManyToOne
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    private Image image;
}
