package pl.agh.ki.tai.photofilter.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Ola on 2016-06-14.
 */
@Data
@XmlRootElement(name = "token")
public class Token {
    @XmlElement
    private String principal;
    @XmlElement
    private String credentials;
}
