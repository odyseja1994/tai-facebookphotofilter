package pl.agh.ki.tai.photofilter.model;

/**
 * Filter enum.
 */
public enum Filter {
    BLUR("Blur"),
    GRAYSCALE("Grayscale"),
    CRYSTALIZE("Crystalize"),
    NOISE("Noise"),
    CHROME("Chrome"),
    HALFTONE("Halftone");

    private String name;


    /** Constructor for enum Filter.
     *
     * @param name String name of the filter.
     */
    Filter(final String name) {
        this.name = name;
    }

    /**
     * Getter for filter's name.
     * @return filter's name
     */
    public String getName() {
        return name;
    }
}
