package pl.agh.ki.tai.photofilter.webapp.service;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Service filtering the images.
 */
public class FilterService {

    /**
     * Method that puts the filter on image.
     * @param srcFile file with original filter.
     * @param filterName name of filter to apply.
     * @return byte[] filtered image
     * @throws IOException when srcFile doesn't exist
     */
    public final byte[] getFilteredImage(final File srcFile, final String filterName) throws IOException {
        BufferedImage srcImage = ImageIO.read(srcFile);
        BufferedImage destImage = deepCopy(srcImage);

        BufferedImageOp filter = FilterFactory.getFilter(filterName);
        if (filter != null) {
            filter.filter(srcImage, destImage);
        }

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        ImageIO.write(destImage, "png", outStream);

        return outStream.toByteArray();
    }

    /** Does deep copy on image.
     *
     * @param srcImage image to deep copy
     * @return deep copied image
     */
    private BufferedImage deepCopy(final BufferedImage srcImage) {
        ColorModel cm = srcImage.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = srcImage.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }
}
