package pl.agh.ki.tai.photofilter.utils;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.SaltedAuthenticationInfo;

import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.model.dao.UserDAO;


/**
 * Realm for Apache Shiro to fetch users from database.
 */
public class MyCustomRealm extends JdbcRealm {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyCustomRealm.class);

    @Override
    protected final AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken token)
            throws AuthenticationException {
        // identify account to LOGGER to
        UsernamePasswordToken userPassToken = (UsernamePasswordToken) token;
        final String username = userPassToken.getUsername();

        if (username == null) {
            LOGGER.warn("Username is null.");
            return null;
        }

        // read password hash and salt from db
        final User user = UserDAO.getUserByLogin(username);

        if (user == null) {
            LOGGER.warn("No account found for user {}", username);
            return null;
        }

        // return salted credentials
        SaltedAuthenticationInfo info = new MySaltedAuthenticationInfo(
                username,
                user.getHashedPassword(),
                user.getSalt()
        );

        return info;
    }
}
