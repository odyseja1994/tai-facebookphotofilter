package pl.agh.ki.tai.photofilter.webapp.rest.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by aleksandra on 27.05.16.
 */
@Data
@XmlRootElement(name = "image")
public class ImageDTO {
    @XmlElement
    private int id;
    @XmlElement
    private String filename;
    @XmlElement
    private byte[] image;
    @XmlElement
    private String nickname;
    @XmlElement
    private byte[] authToken;
}
