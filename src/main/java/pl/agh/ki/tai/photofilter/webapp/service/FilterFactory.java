package pl.agh.ki.tai.photofilter.webapp.service;


import com.jhlabs.image.BoxBlurFilter;
import com.jhlabs.image.ChromeFilter;
import com.jhlabs.image.CrystallizeFilter;
import com.jhlabs.image.GrayscaleFilter;
import com.jhlabs.image.HalftoneFilter;
import com.jhlabs.image.NoiseFilter;
import pl.agh.ki.tai.photofilter.model.Filter;

import java.awt.image.BufferedImageOp;

/**
 * Factory object for filter.
 */
public final class FilterFactory {
    private static final int ADDITIONAL_FILTER_DATA = 3;
    /**
     * Constructor is private because this is utlity class.
     */
    private FilterFactory() {
    }

    /**
     * Returns proper filter object.
     * @param filterName String name of the filter.
     * @return Proper filter object.
     */
    public static BufferedImageOp getFilter(final String filterName) {
        if (filterName.equals(Filter.BLUR.getName())) {
            BoxBlurFilter boxBlurFilter = new BoxBlurFilter();
            boxBlurFilter.setRadius(ADDITIONAL_FILTER_DATA);
            boxBlurFilter.setIterations(ADDITIONAL_FILTER_DATA);
            return boxBlurFilter;
        } else if (filterName.equals(Filter.CHROME.getName())) {
            return new ChromeFilter();
        } else if (filterName.equals(Filter.CRYSTALIZE.getName())) {
            return new CrystallizeFilter();
        } else if (filterName.equals(Filter.GRAYSCALE.getName())) {
            return new GrayscaleFilter();
        } else if (filterName.equals(Filter.HALFTONE.getName())) {
            return new HalftoneFilter();
        } else if (filterName.equals(Filter.NOISE.getName())) {
            return new NoiseFilter();
        }

        return null;
    }

}
