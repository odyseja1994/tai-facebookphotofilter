package pl.agh.ki.tai.photofilter.model.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.utils.HibernateUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * DAO object for User.
 */
public final class UserDAO {

    /**
     * private default constructor for utility class.
     */
    private UserDAO() {
    }

    /**
     * Returns User from database.
     * @param nickname nickname of the user
     * @param password password for the user
     * @return User
     */
    public static User getUserByLoginAndPassword(final String nickname, final String password) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();
        User user = (User) session
                .createQuery("select u from User u where u.nickname=:nickname and u.hashedPassword=:password")
                .setParameter("nickname", nickname)
                .setParameter("password", password).uniqueResult();

        transaction.commit();
        session.close();
        return user;
    }

    /**
     * Returns User from database.
     * @param nickname nickname of the user
     * @param token authentication token for the user
     * @return User
     */
    public static User getUserByLoginAndToken(final String nickname, final byte[] token) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, -1);  // number of days to add
        User user = (User) session
                .createQuery("select u from User u where u.nickname=:nickname "
                        + "and u.authToken=:token and u.lastLogin is not null and u.lastLogin>:date")
                .setParameter("nickname", nickname)
                .setParameter("token", token)
                .setParameter("date", c.getTime()).uniqueResult();

        transaction.commit();
        session.close();
        return user;
    }

    /**
     * Returns User from database.
     * @param nickname nickname of the user
     * @return User
     */
    public static User getUserByLogin(final String nickname) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();
        User user = (User) session
                .createQuery("select u from User u where u.nickname=:nickname")
                .setParameter("nickname", nickname).uniqueResult();
        transaction.commit();
        session.close();
        return user;
    }

    /**
     * Saves user to database.
     * @param user User to save
     * @return id of saved user
     */
    public static Integer saveUser(final User user) {
        return HibernateUtils.saveObject(user);
    }

    /**
     * Updates user on database.
     * @param user User to update
     */
    public static void updateUser(final User user) {
        HibernateUtils.updateObject(user);
    }
}
