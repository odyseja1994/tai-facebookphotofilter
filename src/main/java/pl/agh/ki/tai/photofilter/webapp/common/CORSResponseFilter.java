package pl.agh.ki.tai.photofilter.webapp.common;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Implement a name-bound response filter in cases when you want limit the filter functionality
 * to a matched resource or resource method.
 * **/
@Provider
public class CORSResponseFilter implements ContainerResponseFilter {

    /** Sets Access-Control-Allow-Headers, Access-Control-Allow-Methods, Access-Control-Allow-Origin.
     *
     * @param requestContext ContainerRequestContext
     * @param responseContext ContainerResponseContext
     * @throws IOException
     */
    @Override
    public final void filter(final ContainerRequestContext requestContext,
                             final ContainerResponseContext responseContext) throws IOException {

        responseContext.getHeaders().add("Access-Control-Allow-Headers", "Content-Type");
        responseContext.getHeaders().add("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS");
        responseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
    }
}
