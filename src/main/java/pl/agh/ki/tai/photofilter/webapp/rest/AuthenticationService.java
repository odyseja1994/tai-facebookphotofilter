package pl.agh.ki.tai.photofilter.webapp.rest;

import com.owlike.genson.Genson;
import lombok.Data;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.UnavailableSecurityManagerException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.agh.ki.tai.photofilter.model.Token;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.model.dao.UserDAO;
import pl.agh.ki.tai.photofilter.utils.TokenGenerator;
import pl.agh.ki.tai.photofilter.webapp.rest.dto.UserDTO;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PUT;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.HttpURLConnection;
import java.util.Date;

/**
 * REST Authentication service.
 */
@Path("/authenticate")
public class AuthenticationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationService.class);

    static {
        try {
            SecurityUtils.getSecurityManager();
        } catch (UnavailableSecurityManagerException e) {
            Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");
            SecurityManager securityManager = factory.getInstance();
            SecurityUtils.setSecurityManager(securityManager);
        }
    }

    /**
     * POST /rest/authenticate.
     *
     * @param gotToken Token; Example: {principal: username, credentials: password}.
     * @return Info object
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public final Response authenticateUser(final Token gotToken) {
        LOGGER.info("Got: {}", gotToken);
        Subject currentUser = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken();
        token.setRememberMe(true);
        token.setPassword(gotToken.getCredentials().toCharArray());
        token.setUsername(gotToken.getPrincipal());
        LOGGER.info("Token: {}", token);
        if (!currentUser.isAuthenticated()) {
            try {
                currentUser.login(token);
            } catch (Exception e) {
                LOGGER.error("error: {}", e);
                return Response.status(HttpURLConnection.HTTP_UNAUTHORIZED).build();
            }
        }
        User user = UserDAO.getUserByLogin(gotToken.getPrincipal());
        user.setAuthToken(TokenGenerator.getToken());
        user.setLastLogin(new Date());
        UserDAO.updateUser(user);

        String responseJson = prepareAngularShiroJsonResponse(user.getNickname(), user.getAuthToken());
        LOGGER.info("Returning {}", responseJson);
        return Response.ok(responseJson, MediaType.APPLICATION_JSON).build();
    }

    /**
     * PUT /rest/authenticate.
     *
     * @param userDTO UserDTO; Example: {nickname: nickname, authToken: authToken}.
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public final void logout(final UserDTO userDTO) {
        LOGGER.info("Got: {}", userDTO);
        try {
            User user = UserDAO.getUserByLoginAndToken(userDTO.getNickname(), userDTO.getAuthToken());
            user.setAuthToken(null);
            UserDAO.updateUser(user);
        } catch (Exception e) {
            LOGGER.warn("Cannot logout, user with given token does not exist");
        }

    }

    /**
     * Prepares Info object to return.
     *
     * @param username String user's nickname
     * @param token    byte[] generated authentication token
     * @return Json form of Info
     */
    private String prepareAngularShiroJsonResponse(final String username, final byte[] token) {
        Info info = new Info();
        Authc authc = new Authc();
        Authz authz = new Authz();
        authc.setPrincipal(new Principal(username, token));
        info.setAuthc(authc);
        info.setAuthz(authz);

        return new Genson().serialize(new ResponseObject(info));
    }

    /**
     * Principal object, needed by angular-shiro.
     */
    @Data
    @XmlRootElement(name = "principal")
    public static class Principal {
        @XmlElement
        private String login;
        @XmlElement
        private byte[] token;

        /**
         * Constructor for principal.
         *
         * @param login String user's login.
         * @param token byte[] user's authentication token.
         */
        public Principal(final String login, final byte[] token) {
            this.login = login;
            this.token = token;
        }
    }

    /**
     * Credentials object, needed by angular-shiro.
     */
    @XmlRootElement(name = "credentials")
    public static class Credentials {
    }

    /**
     * Authz object, needed by angular-shiro.
     */
    @Data
    @XmlRootElement(name = "authz")
    public static class Authz {
        @XmlElement
        private String roles = "[ADMIN]";
        @XmlElement
        private String permissions = "*";
    }

    /**
     * Authc object, needed by angular-shiro.
     */
    @Data
    @XmlRootElement(name = "authc")
    public static class Authc {
        private Principal principal;
        private Credentials credentials = new Credentials();
    }

    /**
     * Info object, needed by angular-shiro.
     */
    @Data
    @XmlRootElement(name = "info")
    public static class Info {
        @XmlElement
        private Authc authc;
        @XmlElement
        private Authz authz;
    }

    /**
     * Response object, needed by angular-shiro.
     */
    @Data
    @XmlRootElement(name = "response")
    public static class ResponseObject {
        @XmlElement
        private Info info;

        /**
         * Constructor for ResponseObject.
         *
         * @param info Info object
         */
        public ResponseObject(final Info info) {
            this.info = info;
        }
    }


}
