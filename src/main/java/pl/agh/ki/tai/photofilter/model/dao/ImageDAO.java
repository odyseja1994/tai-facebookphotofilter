package pl.agh.ki.tai.photofilter.model.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.agh.ki.tai.photofilter.model.Image;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.utils.HibernateUtils;

import java.util.List;

/**
 * DAO for Image.
 */
public final class ImageDAO {
    /**
     * private constructor for utility class.
     */
    private ImageDAO() {
    }

    /**
     * Saves given image.
     * @param image image to save
     * @return id of saved image
     */
    public static Integer saveImage(final Image image) {
        return HibernateUtils.saveObject(image);
    }

    /**
     * Return images of given user.
     * @param user user
     * @return List of Images
     */
    public static List<Image> getImagesForUser(final User user) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();
        List<Image> images = (List<Image>) session
                .createQuery("select i from Image i where i.user=:user")
                .setParameter("user", user).list();

        transaction.commit();
        session.close();
        return images;
    }

    /**
     * Fetches from repository image with given id.
     * @param id id of image to fetch
     * @return Image with given id
     */
    public static Image getImage(final Integer id) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();
        Image image = (Image) session
                .createQuery("select i from Image i where i.id=:id")
                .setParameter("id", id).uniqueResult();

        transaction.commit();
        session.close();
        return image;
    }
}
