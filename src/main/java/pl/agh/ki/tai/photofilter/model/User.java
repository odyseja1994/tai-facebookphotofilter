package pl.agh.ki.tai.photofilter.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;

import java.util.Date;
import java.util.List;

/**
 * Database model for User.
 */
@Data
@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = "nickname"),
        @UniqueConstraint(columnNames = "email")})
public class User {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "nickname", unique = true)
    private String nickname;

    @Column(name = "last_login")
    private Date lastLogin;

    @Column(name = "first_login")
    private Date firstLogin;

    @Column(name = "hashed_password")
    private String hashedPassword;

    @Column
    private String salt;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "fb_token")
    private String fbToken;

    @Column(name = "token")
    private byte[] authToken;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Image> images;
}
