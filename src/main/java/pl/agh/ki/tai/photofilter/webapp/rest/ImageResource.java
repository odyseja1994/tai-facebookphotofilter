package pl.agh.ki.tai.photofilter.webapp.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.agh.ki.tai.photofilter.model.Image;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.model.dao.ImageDAO;
import pl.agh.ki.tai.photofilter.model.dao.UserDAO;
import pl.agh.ki.tai.photofilter.utils.RepositoryUtil;
import pl.agh.ki.tai.photofilter.webapp.rest.dto.ImageDTO;
import pl.agh.ki.tai.photofilter.webapp.rest.dto.UserDTO;
import pl.agh.ki.tai.photofilter.webapp.rest.mappers.ImageMapper;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.HttpURLConnection;
import java.util.LinkedList;
import java.util.List;

/**
 * REST Resource for images.
 */
@Path("/images")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ImageResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageResource.class);


    /** Fetches images for given user.
     * POST /rest/images
     * @param userDTO user whose images we want to fetch
     * @return List of images if user is authenticated correctly, 401 otherwise
     */
    @POST
    public final Response getImagesForUser(final UserDTO userDTO) {
        List<ImageDTO> imageDtoList = new LinkedList<>();
        LOGGER.info("{}", userDTO);
        User user = UserDAO.getUserByLoginAndToken(userDTO.getNickname(), userDTO.getAuthToken());
        if (user != null) {
            List<Image> imageList = ImageDAO.getImagesForUser(user);
            for (Image image : imageList) {
                imageDtoList.add(ImageMapper.mapImageToImageDto(image));
            }
            return Response.ok(imageDtoList, MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(HttpURLConnection.HTTP_UNAUTHORIZED).build();
        }

    }

    /**
     * POST /rest/images/create.
     * @param imageDTO image to add
     * @return id of added image if authenticated, 401 otherwise
     */
    @POST
    @Path("/create")
    public final Response addImage(final ImageDTO imageDTO) {
        Image image = new Image();
        User user = UserDAO.getUserByLoginAndToken(imageDTO.getNickname(), imageDTO.getAuthToken());
        if (user != null) {
            image.setUser(user);
            image.setPath(RepositoryUtil.saveImageToRepository(imageDTO.getImage()));
            return Response.ok(ImageDAO.saveImage(image), MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(HttpURLConnection.HTTP_UNAUTHORIZED).build();
        }
    }

    /**
     * Returns given image.
     * GET /rest/images/{imageId}
     * @param imageId id of image.
     * @return image.
     */
    @GET
    @Path("/{imageId}")
    @Produces("image/png")
    public final Response getImg(@PathParam("imageId") final String imageId) {
        Image image = null;
        try {
            image = ImageDAO.getImage(Integer.parseInt(imageId));
            if (image == null) {
                return Response.status(HttpURLConnection.HTTP_NOT_FOUND).build();
            }
        } catch (NumberFormatException e) {
            LOGGER.error(e.getMessage());
        }

        ImageDTO imageDTO = ImageMapper.mapImageToImageDto(image);

        return Response.ok(imageDTO.getImage(), "image/png").build();
    }

    /**
     * Returns object of given image.
     * GET /rest/images/{imageId}/obj
     * @param imageId id of image.
     * @return image.
     */
    @GET
    @Path("/{imageId}/obj")
    @Produces("image/png")
    public final Response getImgObject(@PathParam("imageId") final String imageId) {
        Image image = ImageDAO.getImage(Integer.parseInt(imageId));
        ImageDTO imageDTO = ImageMapper.mapImageToImageDto(image);

        return Response.ok(imageDTO, MediaType.APPLICATION_JSON).build();
    }


}
