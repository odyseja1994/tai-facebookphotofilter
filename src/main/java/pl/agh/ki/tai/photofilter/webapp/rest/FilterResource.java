package pl.agh.ki.tai.photofilter.webapp.rest;


import pl.agh.ki.tai.photofilter.model.Filter;
import pl.agh.ki.tai.photofilter.model.Image;
import pl.agh.ki.tai.photofilter.model.dao.ImageDAO;
import pl.agh.ki.tai.photofilter.utils.RepositoryUtil;
import pl.agh.ki.tai.photofilter.webapp.rest.dto.FilterDTO;
import pl.agh.ki.tai.photofilter.webapp.rest.dto.ImageDTO;
import pl.agh.ki.tai.photofilter.webapp.rest.mappers.ImageMapper;
import pl.agh.ki.tai.photofilter.webapp.service.FilterService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * REST Resource for Filter.
 */
@Path("/filters")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FilterResource {

    /**
     * GET /rest/filters.
     * @return list of available filters.
     */
    @GET
    public final List<FilterDTO> getAllFilters() {
        List<FilterDTO> filterDTOs = new LinkedList<>();

        for (Filter filter : Filter.values()) {
            FilterDTO dto = new FilterDTO();
            dto.setName(filter.getName());
            filterDTOs.add(dto);
        }

        return filterDTOs;
    }

    /**
     * GET /rest/filters/{filterName}/{imageId}.
     * @param filterName Filter to apply
     * @param imageId id of image
     * @return image with given filter
     */
    @GET
    @Path("/{filterName}/{imageId}")
    public final ImageDTO makeFilter(@PathParam("filterName") final String filterName,
                                     @PathParam("imageId") final String imageId) {
        Image img = ImageDAO.getImage(Integer.parseInt(imageId));
        File file = new File(img.getPath());
        Filter usedFilter = null;
        byte[] destImage = null;
        FilterService filterService = new FilterService();

        try {
            destImage = filterService.getFilteredImage(file, filterName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Filter filter : Filter.values()) {
            if (filter.getName().equals(filterName)) {
                usedFilter = filter;
            }
        }

        Image filteredImage = new Image();
        filteredImage.setFilter(usedFilter);
        filteredImage.setPath(RepositoryUtil.saveImageToRepository(destImage));

        img.getImagesWithFilters().add(filteredImage);

        return ImageMapper.mapImageToImageDto(filteredImage);
    }


}
