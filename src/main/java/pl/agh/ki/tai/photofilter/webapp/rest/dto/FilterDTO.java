package pl.agh.ki.tai.photofilter.webapp.rest.dto;


import lombok.Data;

/**
 * DTO object for filter.
 */
@Data
public class FilterDTO {
    private String name;
}
