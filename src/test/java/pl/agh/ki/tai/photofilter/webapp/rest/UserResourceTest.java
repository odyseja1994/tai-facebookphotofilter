package pl.agh.ki.tai.photofilter.webapp.rest;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.agh.ki.tai.photofilter.model.Token;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.model.dao.TestObjectFactory;
import pl.agh.ki.tai.photofilter.model.dao.UserDAO;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

/**
 * Created by Ola on 2016-06-24.
 */
@RunWith(Arquillian.class)
@RunAsClient
public class UserResourceTest {
    private User user;
    private WebTarget target;

    @Deployment
    public static WebArchive createDeployment() {
        return WebappTestHelper.deploy(UserResource.class);
    }

    @Before
    public void before() {
        // setup the session factory
        target = WebappTestHelper.setupSession();
        user = TestObjectFactory.getUser();
    }

    @Test
    public void addUser() throws Exception {
        Token token = new Token();
        token.setPrincipal(user.getNickname());
        token.setCredentials(user.getHashedPassword());
        Response response = target.path("rest").path("users").request().accept(MediaType.APPLICATION_JSON).post(Entity.json(token));
        assertEquals(201, response.getStatus());
        User addedUser = UserDAO.getUserByLogin(token.getPrincipal());
        assertEquals(token.getPrincipal(), addedUser.getNickname());
    }

}