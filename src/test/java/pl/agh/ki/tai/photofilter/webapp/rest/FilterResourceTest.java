package pl.agh.ki.tai.photofilter.webapp.rest;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Ola on 2016-06-27.
 */
@RunWith(Arquillian.class)
@RunAsClient
public class FilterResourceTest {
    private WebTarget target;

    @Deployment
    public static WebArchive createDeployment() {
        return WebappTestHelper.deploy(ImageResource.class);
    }

    @Before
    public void before() {
        target = WebappTestHelper.setupSession();

    }

    @Test
    public void getAllFilters() throws Exception {
        Response response = target.path("rest").path("filters").request().accept(MediaType.APPLICATION_JSON).get();
        assertEquals(200, response.getStatus());
        List filters = response.readEntity(List.class);
        assertNotNull(filters);
        assertNotEquals(0, filters.size());
    }
}