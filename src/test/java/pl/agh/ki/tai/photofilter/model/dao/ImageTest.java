package pl.agh.ki.tai.photofilter.model.dao;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import pl.agh.ki.tai.photofilter.model.Image;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.utils.HibernateUtils;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Ola on 2016-05-22.
 */
public class ImageTest {
    private User user;

    @Before
    public void before() {
        // setup the session factory
        SessionFactory sessionFactory = TestObjectFactory.getSessionFactory();
        HibernateUtils.setSessionFactory(sessionFactory);
        user = TestObjectFactory.getUser();
        Integer id = UserDAO.saveUser(user);
        user.setId(id);
    }

    @Test
    public void saveImageTest() {
        Integer id = persistImageHelper().getId();
        assertNotNull(id);
        assertTrue(id >= 0);
    }

    @Test
    public void getImageTest() {
        Integer id = persistImageHelper().getId();
        Image image = ImageDAO.getImage(id);
        assertEquals(id, (Integer) image.getId());
    }

    @Test
    public void getImagesForUserTest() {
        Image image = persistImageHelper();
        List<Image> addedImage = ImageDAO.getImagesForUser(user);
        assertEquals(image.getPath(), addedImage.get(0).getPath());
        assertEquals(1, addedImage.size());
    }



    private Image persistImageHelper() {
        Image image = TestObjectFactory.getImage();
        image.setUser(user);
        image.setId(ImageDAO.saveImage(image));
        return image;
    }
}