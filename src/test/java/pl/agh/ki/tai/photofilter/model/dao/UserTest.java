package pl.agh.ki.tai.photofilter.model.dao;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.utils.HibernateUtils;

import static org.junit.Assert.*;

/**
 * Created by Ola on 2016-05-18.
 */
public class UserTest {
    private SessionFactory sessionFactory;

    @Before
    public void before() {
        // setup the session factory
        sessionFactory = TestObjectFactory.getSessionFactory();
        HibernateUtils.setSessionFactory(sessionFactory);
    }

    @Test
    public void addUserTest() {
        User user = TestObjectFactory.getUser();
        Integer id = UserDAO.saveUser(user);
        assertNotNull(id);
        assertTrue(id >= 0);

        User addedUser = UserDAO.getUserByLoginAndPassword(user.getNickname(), user.getHashedPassword());

        assertEquals(user.getEmail(), addedUser.getEmail());
        assertEquals(user.getNickname(), addedUser.getNickname());
        assertEquals(user.getHashedPassword(), addedUser.getHashedPassword());
    }

    @Test
    public void getUserByLoginAndPasswordTest() {
        User user = TestObjectFactory.getUser();
        user.setId(UserDAO.saveUser(user));

        User addedUser = UserDAO.getUserByLoginAndPassword(user.getNickname(), user.getHashedPassword());

        assertEquals(user.getId(), addedUser.getId());
        assertEquals(user.getEmail(), addedUser.getEmail());
        assertEquals(user.getNickname(), addedUser.getNickname());
        assertEquals(user.getHashedPassword(), addedUser.getHashedPassword());
    }

}