package pl.agh.ki.tai.photofilter.model.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import pl.agh.ki.tai.photofilter.model.Image;
import pl.agh.ki.tai.photofilter.model.User;

import java.util.Date;

/**
 * Created by Ola on 2016-05-22.
 */
public class TestObjectFactory {
    private static final String TEST_EMAIL = "aaa@bbb.com";
    private static final String TEST_LOGIN = "aaaa";
    private static final String TEST_PASS = "ccc";
    private static final String PATH1 = "C:\\";
    private static final String PATH2 = "D:\\";

    public static User getUser() {
        User user = new User();
        user.setEmail(TEST_EMAIL);
        user.setNickname(TEST_LOGIN);
        user.setFirstLogin(new Date());
        user.setHashedPassword(TEST_PASS);
        user.setLastLogin(new Date());
        return user;
    }

    public static Image getImage() {
        Image image = new Image();
        image.setPath(PATH1);
        return image;
    }

    public static SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(User.class)
                .addAnnotatedClass(Image.class).configure();
        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        configuration.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        configuration.setProperty("hibernate.connection.url", "jdbc:h2:./mem");
        configuration.setProperty("hibernate.hbm2ddl.auto", "create");
        return configuration.buildSessionFactory();
    }

}
