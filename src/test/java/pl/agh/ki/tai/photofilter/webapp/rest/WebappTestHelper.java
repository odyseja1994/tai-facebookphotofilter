package pl.agh.ki.tai.photofilter.webapp.rest;

import org.hibernate.SessionFactory;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import pl.agh.ki.tai.photofilter.model.dao.TestObjectFactory;
import pl.agh.ki.tai.photofilter.utils.HibernateUtils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Created by Ola on 2016-06-24.
 */
public class WebappTestHelper {
    public static final String BASE_URL = "http://127.0.0.1:8889/test";

    public static URI getBaseUri() {
        return UriBuilder.fromUri(BASE_URL).build();
    }

    public static WebArchive deploy(Class resource) {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addClass(resource)
                .addAsManifestResource("arquillian.xml")
                .setWebXML("web.xml");
    }

    public static WebTarget setupSession() {
        SessionFactory sessionFactory = TestObjectFactory.getSessionFactory();
        HibernateUtils.setSessionFactory(sessionFactory);
        Client client = ClientBuilder.newClient();
        return client.target(WebappTestHelper.getBaseUri());
    }
}
