package pl.agh.ki.tai.photofilter.webapp.rest;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.agh.ki.tai.photofilter.model.Token;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.model.dao.TestObjectFactory;
import pl.agh.ki.tai.photofilter.model.dao.UserDAO;
import pl.agh.ki.tai.photofilter.webapp.rest.dto.UserDTO;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

/**
 * Created by Ola on 2016-06-26.
 */
@RunWith(Arquillian.class)
@RunAsClient
public class AuthenticationServiceTest {
    private WebTarget target;
    private User user;
    private Token token;

    @Deployment
    public static WebArchive createDeployment() {
        return WebappTestHelper.deploy(AuthenticationService.class);
    }

    @Before
    public void before() {
        // setup the session factory
        target = WebappTestHelper.setupSession();

        //initialize user in database
        user = TestObjectFactory.getUser();
        token = new Token();
        token.setPrincipal(user.getNickname());
        token.setCredentials(user.getHashedPassword());
        target.path("rest").path("users").request().accept(MediaType.APPLICATION_JSON).post(Entity.json(token));
    }

    @Test
    public void authenticateAndLogoutUser() throws Exception {
        assertNull(user.getAuthToken()); //Check if token is not set
        //LOGIN USER
        Response response = target.path("rest").path("authenticate").request().accept(MediaType.APPLICATION_JSON).post(Entity.json(token));
        assertEquals(200, response.getStatus());
        assertNotNull(response.getEntity());
        user = UserDAO.getUserByLogin(user.getNickname());
        assertNotNull(user.getAuthToken()); //Check if token is set

        AuthenticationService.Info info = response.readEntity(AuthenticationService.ResponseObject.class).getInfo();
        AuthenticationService.Principal principal = info.getAuthc().getPrincipal();
        byte[] authToken = principal.getToken();

        //LOGOUT USER
        UserDTO userDTO = new UserDTO();
        userDTO.setNickname(user.getNickname());
        userDTO.setAuthToken(authToken);

        target.path("rest").path("authenticate").request().accept(MediaType.APPLICATION_JSON).put(Entity.json(userDTO));
        user = UserDAO.getUserByLogin(user.getNickname());
        assertNull(user.getAuthToken());
    }


}