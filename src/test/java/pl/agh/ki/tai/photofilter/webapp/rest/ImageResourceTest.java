package pl.agh.ki.tai.photofilter.webapp.rest;

import org.apache.commons.io.FileUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.agh.ki.tai.photofilter.model.Token;
import pl.agh.ki.tai.photofilter.model.User;
import pl.agh.ki.tai.photofilter.model.dao.TestObjectFactory;
import pl.agh.ki.tai.photofilter.model.dao.UserDAO;
import pl.agh.ki.tai.photofilter.webapp.rest.dto.ImageDTO;
import pl.agh.ki.tai.photofilter.webapp.rest.dto.UserDTO;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Ola on 2016-06-26.
 */
@RunWith(Arquillian.class)
@RunAsClient
public class ImageResourceTest {
    private String username;
    private byte[] authToken;
    private WebTarget target;

    @Deployment
    public static WebArchive createDeployment() {
        return WebappTestHelper.deploy(ImageResource.class);
    }

    @Before
    public void before() {
        // setup the session factory
        target = WebappTestHelper.setupSession();

        //initialize user in database
        Token token = new Token();
        User user = TestObjectFactory.getUser();
        username = user.getNickname();
        token.setPrincipal(user.getNickname());
        token.setCredentials(user.getHashedPassword());
        target.path("rest").path("users").request().accept(MediaType.APPLICATION_JSON).post(Entity.json(token));
        Response response = target.path("rest").path("authenticate").request().accept(MediaType.APPLICATION_JSON).post(Entity.json(token));
        assertEquals(200, response.getStatus());
        assertNotNull(response.getEntity());
        user = UserDAO.getUserByLogin(user.getNickname());
        assertNotNull(user.getAuthToken()); //Check if token is set

        AuthenticationService.Info info = response.readEntity(AuthenticationService.ResponseObject.class).getInfo();
        AuthenticationService.Principal principal = info.getAuthc().getPrincipal();
        authToken = principal.getToken();
    }

    @After
    public void after() throws IOException {
        FileUtils.cleanDirectory(new File("repo"));
    }

    @Test
    public void getImagesForUser() throws Exception {
        UserDTO user = new UserDTO();
        user.setNickname(username);
        user.setAuthToken(authToken);
        Response response = target.path("rest").path("images").request().accept(MediaType.APPLICATION_JSON).post(Entity.json(user));
        assertEquals(200, response.getStatus());
        assertEquals(new LinkedList<>(), response.readEntity(List.class));
    }

    @Test
    public void addImage() throws Exception {
        byte[] arr = FileUtils.readFileToByteArray(new File("sample/Star.png"));

        UserDTO user = new UserDTO();
        user.setNickname(username);
        user.setAuthToken(authToken);

        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setAuthToken(authToken);
        imageDTO.setImage(arr);
        imageDTO.setNickname(username);
        imageDTO.setFilename("filename.jpg");

        Response response = target.path("rest").path("images").path("create").request().accept(MediaType.APPLICATION_JSON).post(Entity.json(imageDTO));
        assertEquals(200, response.getStatus());

        response = target.path("rest").path("images").request().accept(MediaType.APPLICATION_JSON).post(Entity.json(user));
        List images = response.readEntity(List.class);
        assertNotNull(images);
        assertEquals(1, images.size());
        assertEquals(200, response.getStatus());
    }

}